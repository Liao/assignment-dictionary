asm = nasm -felf64
ld = ld -o
all: main

lib.o: lib.asm
	$(asm) $<

dict.o: dict.asm
	$(asm) $<

main.o:main.asm colon.inc words.inc
	$(asm) $<

main: main.o dict.o lib.o
	$(ld) $@ $^
clean: main main.o dict.o lib.o
	rm main
	rm main.o
	rm dict.o
	rm lib.o
