	 	 	 	
global find_word
extern string_equals

section .text
;rdi pointer to null terminated key string(which serching for) 
;rsi pointer to the last word(start) 
find_word:
.loop:
	test rsi, rsi
	je .fail
	push rsi
	push rdi
	add rsi, 8	
	call string_equals
	pop rdi 
	pop rsi
	cmp rax, 1
	je .success
	mov rsi, [rsi]
	jmp .loop
.success:	
	mov rax, rsi
	ret
.fail:
	mov rax,0
	ret	

