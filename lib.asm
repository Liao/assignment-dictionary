%define stderr 2
%define stdout 1
global exit
global string_length
global print_string 
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word 
global parse_int
global parse_uint
global string_copy
global print_err
global print_char
section .text 

exit:
	mov rax, 60
	syscall

string_length:
	xor rax, rax
	.count:
		cmp byte [rdi+rax], 0 ;take parametre from rdi  
		je .finish
		add rax, 1 
		jmp .count
	.finish:
	ret

print_string:
	push rdi
	call string_length
	pop rsi	
	mov rdx, rax
	mov rax, 1
	mov rdi, stdout
	syscall
	ret

print_err:
	push rdi
	call string_length
	pop rsi
	mov rdi, stderr
	mov rdx, rax	
	mov rax, 1 
	syscall
	ret

print_char:
	dec rsp
	mov byte [rsp], dil
	mov rsi, rsp 
	mov rax, 1
	mov rdi, stdout
	mov rdx, 1
	syscall
	inc rsp	
	ret

print_newline:
	dec rsp	
	mov byte [rsp], 0xA	
	mov rdi, [rsp] 
	call print_char
	inc rsp	
	ret

print_uint:
	mov rbp,rsp	
	mov rax, rdi; get parametre from rdi 
	mov rbx, 10 
	dec rsp
	mov byte [rsp], 0 
	.save_loop:
		xor rdx, rdx
		div rbx 
		add rdx, 0x30	
		dec rsp
		mov byte [rsp], dl	
		cmp rax, 0
		jne .save_loop
	.print:
		mov rdi, rsp
		call print_string
		mov rsp, rbp
	ret

print_int:
	cmp rdi, 0   
	jnge .negative ;if parametre is not negative( 0 | >0) then just call print_uint
	call print_uint 
	jmp .end	
	.negative:
		push rdi 	
		mov rdi, 0x2D 
		call print_char
		pop rdi	
		not rdi
		inc rdi
		call print_uint	
	.end:
		ret 

string_equals:
	xor rcx, rcx	
	.loop:
		push rdi
		push rsi
		mov rsi, [rsi]
		mov rdi, [rdi]
		cmp sil, dil
		jne .ne
		cmp sil, 0
		je .e	
		pop rsi
		pop rdi
		inc rsi
		inc rdi	
		jmp .loop	
	.ne:
		pop rsi
		pop rdi	
		xor rax, rax
		ret	
	.e:
		pop rsi
		pop rdi	
		mov rax, 1
		ret 	

read_char:
	mov rax, 0
	mov rdi, 0
	push 0 ; prepare a place for read char 
	mov rsi, rsp; address of prepared place 
	mov rdx, 1 
	syscall 
	pop rax
	cmp rax, 10
	jz .null 
	jmp .end 
	.null:
		mov rax, 0 
	.end:
		ret	

parse_uint:
	xor rax, rax
	xor rcx, rcx
	xor rdx, rdx; for using mul	
	mov r8, 10	
	.loop:
		mov rsi, [rdi+rcx]
		and rsi, 0xff	
		cmp rsi, 48
		jb .return
		cmp rsi, 57
		jg .return
		inc rcx
		sub rsi, 48
		mul r8 
		add rax, rsi  
		jmp .loop	
	.return:
		mov rdx, rcx 
		ret 
	
parse_int:
	push rbp
	mov rbp, rsp
	sub rsp, 24
	mov qword [rbp-8], rdi ; address
	call string_length
	mov qword [rbp-16], rax; length 
	mov qword [rbp-24], 0; have symbol? 
	.judge:
		mov rax, [rbp-8]
		mov rax, [rax]	
		and rax, 0xff
		cmp rax, 45
		je .neg
		cmp rax, 43
		jne .posi
		mov rax, 1 
		mov qword [rbp-24], rax	
		mov rax, [rbp-8]
		mov rax, [rax+1]
		cmp al, 32;" " or not 
		je .end 	
	.posi:
		mov rax, [rbp-24]
		cmp rax, 1
		je .sym
		mov rdi, [rbp-8]
		call parse_uint
		jmp .end
		.sym:
			mov rdi, [rbp-8]
			inc rdi
			call parse_uint
			inc rdx 	
			jmp .end	
	.neg:
		mov rax, [rbp-8]
		mov rax, [rax+1]
		and rax, 0xff	
		xor rdx, rdx
		cmp rax, 32	
		je .end
		mov rdi, [rbp-8]
		inc rdi
		call parse_uint
		not rax
		inc rax
		inc rdx	
	.end:
		mov rsp, rbp
		pop rbp 
		ret 

read_word:
	.check:
	 	push rdi;address	
		push rsi;length 	
		call read_char
		pop rsi
		pop rdi	
		cmp rax, 0x20
		je .check 
		cmp rax, 0x9
		je .check 
		cmp rax, 0xA 
		je .check 
		xor rdx, rdx	
	.loop:
		cmp rdx, rsi
		je .wrong 	
		test rax, rax
		je .end		
		cmp rax, 0x20
		je .end
		cmp rax, 0xA 
		je .end
		cmp rax, 0x9
		je .end
		mov byte [rdi+rdx], al 
		inc rdx
		push rdx
		push rdi
		push rsi	
		call read_char	
		pop rsi
		pop rdi
		pop rdx  
		jmp .loop	
	.wrong:
		xor rax, rax
		ret	
	.end:
		mov rax, rdi	
		mov byte [rdi + rdx], 0 
		ret

string_copy:
	push rbp 
	mov rbp, rsp
	sub rsp, 32 
	mov qword [rbp-8],rdi; address of string 
	mov qword [rbp-16], rdx ; length of buffer 
	mov qword [rbp-24], rsi ; address of buffer  
	call string_length
	mov qword [rbp-32], rax; length of string
	mov rcx, [rbp-16]
	dec rcx	
	cmp rax, rcx 
	jg .Err
	mov rcx, [rbp-32]
	mov rdi, [rbp-24]
	mov qword [rdi+rcx], 0 
	.loop:	
		dec rcx
		mov rdx, [rbp-8]
		mov rdx, [rdx+rcx]	
		mov qword [rdi+rcx], rdx
		cmp rcx,0
		jne .loop 
	mov rax, [rbp-32]	
	jmp .end	
	.Err:
		xor rax, rax 
	.end:
		mov rsp, rbp
		pop rbp
		ret 
