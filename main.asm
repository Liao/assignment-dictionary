%include "lib.inc"
%include "words.inc"
%define stderr 2
extern find_word
global _start
section .data
word_not_found: db "In dictionary no such a word",10,0 
word_too_long: db "Entry a word,whose length is not bigger than 255",10,0
section .text 
_start:
	push rbp,
	mov rbp, rsp
	sub rsp,256
	call print_char
	mov rdi,rbp
	call read_word
	mov rdi, rbp
	call string_length  
	cmp rax, 255  
	jg .err	
	mov rsi,dictionary
	call find_word	
	test rax, rax
	je .not_found
	.found:	
	mov rdi, rax
	add rdi, 8 
	push rdi
	call string_length
	pop rdi
	add rdi, rax
	inc rdi
	call print_string	
	mov rdi,0
	jmp .end	
	.err:
	mov rdi, word_too_long
	call print_err
	mov rdi,1
	jmp .end
	.not_found:
	mov rdi, word_not_found
	call print_err	
	mov rdi,1
	.end:	
	mov rsp,rbp
	pop rbp
	call print_newline	
	call exit 
