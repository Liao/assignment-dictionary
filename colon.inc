%define dictionary 0

%macro colon 2
	%ifstr %1
		%ifid %2
			%%next_word:dq dictionary
			db %1,0
			%2:
				%define dictionary %%next_word
		%else
			%error "the second argument(label) should be a string without quotes"
		%endif 
	%else
		%error "the first argument(key) should be a string in quotes"
	%endif
%endmacro
; the structrue just like label: pointer, key	
